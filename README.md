# Recipe-app-proxy

NGINX proxy app for our recipe app API
UDEMY Course = DevOps Deployment Automation with Terraform, AWS and Docker

## Usage

### Env vars
* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - hostname to forward req to (default: `app)`
* `APP_PORT` - port to forward req to (default: `9000`)